/*----------------------------------------------------------------------------*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
/*----------------------------------------------------------------------------*/
#include "my1sockbase.h"
#include "my1keys.h"
/*----------------------------------------------------------------------------*/
#define BUFFSIZE 1024
#define DEFAULT_PORT 1337
/*----------------------------------------------------------------------------*/
int main(int argc, char* argv[])
{
	int loop, port=DEFAULT_PORT;
	int size, test;
	char buff[BUFFSIZE];
	char host_default[] = "localhost";
	char *phost = host_default, *pipv4 = 0x0;
	my1sock_t sock;
	my1host_t host;
	my1key_t ikey;
	/* process parameters */
	for (loop=1;loop<argc;loop++)
	{
		if (argv[loop][0]=='-'&&argv[loop][1]=='-')
		{
			if (!strcmp(&argv[loop][2],"host"))
			{
				loop++;
				phost = argv[loop];
			}
			else if (!strcmp(&argv[loop][2],"port"))
			{
				loop++;
				port = atoi(argv[loop]);
			}
			else if (!strcmp(&argv[loop][2],"ip"))
			{
				loop++;
				pipv4 = argv[loop];
			}
			else
			{
				printf("Unknown option '%s'! Aborting!\n",argv[loop]);
				exit(1);
			}
		}
		else
		{
			printf("Unknown parameter '%s'! Aborting!\n",argv[loop]);
			exit(1);
		}
	}
	/* check host */
	if (pipv4) host_from_ip(&host,pipv4,port);
	else host_from_name(&host,phost,port);
	if (host.flag&HOST_FLAG_NAME_VALID)
		printf("@@ Host: %s@%s(%d)\n",host.addr,host.name,host.port);
	else
	{
		printf("** Invalid host (%s|%s)!\n",phost,pipv4);
		exit(1);
	}
	/** dummy loop */
	sock_init(&sock);
	do
	{
		printf("-- Creating socket\n");
		sock_create(&sock);
		if (sock.sock==INVALID_SOCKET)
		{
			printf("** Socket create error! (%d)\n",sock.flag);
			break;
		}
		/* set remote host info */
		sock.addr.sin_family = AF_INET;
		sock.addr.sin_port = htons(host.port);
		sock.addr.sin_addr = host.iadd;
		printf("-- Prepare client socket\n");
		sock_client(&sock);
		if (sock.flag&SOCKET_ERROR)
		{
			printf("** Socket client error (%08x)!\n",sock.flag);
			break;
		}
		printf("-- Reading...\n");
		printf("## Press <ESC> to exit.\n");
		size = BUFFSIZE;
		while(1)
		{
			ikey = get_keyhit();
			if (ikey==KEY_ESCAPE||ikey=='q'||ikey=='Q')
			{
				printf("\nExit requested (0x%08X)\n",ikey);
				shutdown(sock.sock,2);
				break;
			}
			test = sock_peek(sock.sock,size,(void*)buff);
			if (test<0)
			{
				printf("\nSocket Error? (%d)\n",test);
				break;
			}
			else if (test>0)
			{
				loop = 0;
				while (buff[loop]!='$'&&loop<size-1) loop++;
				if (buff[loop]=='$') buff[loop+1] = 0x0;
				printf("Buff:%s\n",buff);
			}
		}
		printf("-- Closing socket\n");
		sock_finish(&sock,1);
	}
	while(0); /* end of dummy loop */
	sock_free(&sock);
	return 0;
}
/*----------------------------------------------------------------------------*/
