/*----------------------------------------------------------------------------*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <time.h>
/*----------------------------------------------------------------------------*/
#include "my1sockbase.h"
#include "my1keys.h"
/*----------------------------------------------------------------------------*/
#define BUFFSIZE 1024
#define DEFAULT_PORT 1337
/*----------------------------------------------------------------------------*/
int main(int argc, char* argv[])
{
	int loop, port=1337;
	int size, test, wait;
	char buff[BUFFSIZE];
	my1sock_t sock;
	my1conn_t ccon;
	my1key_t ikey;
	/* process parameters */
	for (loop=1;loop<argc;loop++)
	{
		if (argv[loop][0]=='-'&&argv[loop][1]=='-')
		{
			if (!strcmp(&argv[loop][2],"port"))
			{
				loop++;
				port = atoi(argv[loop]);
			}
			else
			{
				printf("** Unknown option '%s'! Aborting!\n",argv[loop]);
				exit(1);
			}
		}
		else
		{
			printf("** Unknown parameter '%s'! Aborting!\n",argv[loop]);
			exit(1);
		}
	}
	/* prepare random */
	srandom((unsigned int)time(0x0));
	/** dummy loop */
	sock_init(&sock);
	do
	{
		printf("-- Creating socket (Port:%d)\n",port);
		sock_create(&sock);
		if (sock.sock==INVALID_SOCKET)
		{
			printf("** Socket create error! (%d)\n",sock.flag);
			break;
		}
		/* set self host info */
		sock.addr.sin_family = AF_INET;
		sock.addr.sin_port = htons(port);
		sock.addr.sin_addr.s_addr = htonl(INADDR_ANY);
		printf("-- Prepare server socket\n");
		sock_server(&sock,1);
		if (sock.flag&SOCKET_ERROR)
		{
			printf("** Socket server error (%08x)!\n",sock.flag);
			break;
		}
		/* prepare client connection socket */
		wait = 1;
		ccon.alen = sizeof(struct sockaddr);
		/* start listening for clients */
		printf("-- Listening at port %d!\n",port);
		printf("## Press <ESC> to exit.\n");
		while(1)
		{
			ikey = get_keyhit();
			if (ikey==KEY_ESCAPE||ikey=='q'||ikey=='Q')
			{
				printf("\n@@ Exit requested (0x%08X)\n",ikey);
				break;
			}
			if (wait)
			{
				test = sock_accept(&sock,&ccon);
				if (!test) continue;
				else if (test<0)
				{
					printf("** Socket accept error!\n");
					break;
				}
				/* should create new thread for this? */
				printf("@@ Connection from {%s}!\n",ccon.addr);
				wait = 0;
			}
			sprintf(buff,"#%d$",(int)(random()%100)+1);
			size = strlen(buff);
			test = sock_send(ccon.sock,size,(void*)buff);
			if (test<0)
			{
				printf("** Socket error (closed?)! (%d)!\n",test);
				sock_done(ccon.sock,1);
				wait = 1;
				continue;
			}
			else if (test!=size)
			{
				/* error? abort! */
				printf("** Cannot complete data send! (%d/%d)!\n",
					test,size);
				break;
			}
			printf("@@ Sent {%s}[%d/%d]\n",buff,test,size);
			fflush(stdout);
			sleep(3);
		}
		if (!wait)
		{
			/* clean up connection */
			printf("@@ Closing connection from {%s}!\n",ccon.addr);
			sock_done(ccon.sock,1);
		}
		printf("-- Closing socket\n");
		sock_finish(&sock,1);
	}
	while(0); /* end of dummy loop */
	sock_free(&sock);
	return 0;
}
/*----------------------------------------------------------------------------*/
